CREATE TABLE IF NOT EXISTS urls (
   url_id VARCHAR(48) primary key,
   original_url TEXT NOT NULL,
   created_at TIMESTAMP NOT NULL
);