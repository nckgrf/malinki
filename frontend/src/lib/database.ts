import pg from "pg";
const { Pool } = pg;

const {
    DB_USER,
    DB_HOST,
    DB_NAME,
    DB_PW,
    DB_PORT,
} = process.env;


const pool = new Pool({
    user: DB_USER,
    host: DB_HOST,
    database: DB_NAME,
    password: DB_PW,
    port: Number(DB_PORT)
});

export {pool};