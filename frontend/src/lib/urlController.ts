import {pool} from "$lib/database";
import { nanoid } from "nanoid";

const getUrlById = (urlID: string) => {
    return pool.query("select original_url from urls u where u.url_id = $1", [urlID]);
}

const addUrl = (originalUrl: string) => {
    const id = nanoid(8);
    return pool.query("insert into urls(url_id, original_url, created_at) VALUES ($1, $2, NOW())", [id, originalUrl]).then(() => {
        return Promise.resolve(id);
    });
}

export { addUrl, getUrlById };