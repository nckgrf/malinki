import {getUrlById} from "$lib/urlController";

export async function GET(event: any) {
    const {id} = event.params;
    const {rows} = await getUrlById(id);
    let location = rows.length > 0 ? rows[0].original_url : "/not-found";

    return new Response(null, {
        status: 302,
        headers: {
            location: location
        }
    })
}