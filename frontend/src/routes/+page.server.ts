import type {RequestEvent} from "@sveltejs/kit";
import {addUrl} from "$lib/urlController";
import {json} from "@sveltejs/kit";

export const actions = {
    default: async (event: RequestEvent
    ) => {
        const data = await event.request.formData();
        const urlValue = data.get("url") as string;
        const uniqueID = await addUrl(urlValue);
        return {uniqueID}
    }
};